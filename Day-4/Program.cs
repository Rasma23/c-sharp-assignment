﻿// See https://aka.ms/new-console-template for more information
 // 1. Excersise Compare three numbers a = 200, b = 35, c = 30
using System;
namespace HelloWorld
{
  class Hello
  {
    static void Main(){
        int a=200;
        int b=35;
        int c=30;
        if(a>b&&a>c){
            Console.WriteLine("a is bigger");
        }else if(b>a&&b>c){
            Console.WriteLine("b is bigger");
        }else if(c>a&&c>b){
            Console.WriteLine("c is bigger");
        }else {
            Console.WriteLine("All are equal");
        }
   // 2. Excersise Compare three numbers a = 20, b = 30, c = 40
            int A= 20;
            int B=30;
            int C=40;
            var result =A>B&&A>C?"a is bigger":
                           B>A&&B>C?"b is bigger":
                              C>A&&C>B?"c is bigger":"all are equal";
                Console.WriteLine(result);  
  //  3. Excersise Print odd number 1, 3, 5, 7, 9  
             for(int i=0;i<=10;i++) {
                 if(i%2==1){
                     Console.WriteLine(i);
                 }

             }
//4. Excersise Do not print Sunday and Saturday
       string[] weekDays = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    

             foreach (string day in weekDays) 
              {
                 if(day == "Sunday"){
                     continue;
                 }
                 else if(day == "Saturday"){
                     continue;
                 }
                 Console.WriteLine(day);
                

             }

             //switch case:
            char letters= 'o';
            switch(letters){
                 case 'e':
                 Console.WriteLine("Vowels");
                 break;
                 case 'a':
                 Console.WriteLine("Vowels");
                  break;
                  case'i':
                 Console.WriteLine("Vowels");
                 break;
                  case'o':
                  Console.WriteLine("Vowels");
                 break;
                  case'u':
                  Console.WriteLine("Vowels");
                  break;

                  default:
                  Console.WriteLine("Consonants");
                  break;
            }
            //Random class:
            Random rnd = new Random();
            
            for(int i=1;i<=5;i++)
            Console.WriteLine(rnd.Next(100,200));
    
     }
  }
}

