﻿using System;
//namespace MyProgram

 public class Program
 {
     public  static void Main(){
         //Excersise 1
       var list = new List<String>();
		
		while(true)
		{
			Console.WriteLine("Write a name:");				
			var input = Console.ReadLine();
			if(String.IsNullOrWhiteSpace(input))
				break;
			else
				list.Add(input);
			
		}
		
		switch(list.Count)
		{
			case 0:
				Console.WriteLine("Nobody likes your post");
				break;
			case 1:
				Console.WriteLine("{0} likes your post", list[0]);
				break;
			case 2:
				Console.WriteLine("{0} and {1} liked your post", list[0], list[1]);
				break;
			default:
				Console.WriteLine("{0} and {1} and {2} other people liked your post", list[0], list[1], (list.Count - 2));
				break;
		}
 //Excersise 2   
         var name = Console.ReadLine();
         Console.WriteLine("Username is " + name);
            Console.WriteLine("Enter Name : ");
            string originalString = Console.ReadLine();
            string reverseString = string.Empty;
            for (int i = originalString.Length - 1; i >= 0; i--)
            {
                reverseString += originalString[i];
            }
            Console.Write("Reverse String is : {reverseString} ");
             Console.ReadLine();
             
//Excersise 3
        var list = new List<int>();
		
        		while(list.Count < 5)
        		{
        			Console.WriteLine("Type 5 unique numbers:");
        			var input = Convert.ToInt32(Console.ReadLine());
			
        			if(list.Contains(input))
        				Console.WriteLine("Error! Number already in list!");
       			else
       				list.Add(input);
       		}
		
      		list.Sort();
     		foreach(var number in list)
    		    {
     			Console.Write("{0} ", number);
   		    }
//Excersise 4
var unique = new List<int>();
		
		while(true)
		{
			Console.WriteLine("Enter a number or type quit to leave");
			var input = Console.ReadLine();
			
			if(input.CompareTo("quit") == 0)
				break;
			else
			{
				var number = Convert.ToInt32(input);
				if(unique.Contains(number))
					continue;
				else
					unique.Add(number);
			}
		}
		
 		foreach(var output in unique)
 			Console.Write("{0} ", output); 
//Excersise 5
            var list = new List<int>();
		while(true)
		{
			Console.WriteLine("Type a list of number, separated by commas");
			var input = Console.ReadLine();
		
			var array = input.Split(',');

			if((input.Length == 0 || input.Length < 5))
			{
				Console.WriteLine("Invalid list! Try again.");
			}
			else
			{
				foreach(var number in array)
					list.Add(Convert.ToInt32(number));
				break;
			}
		}
		list.Sort();
		
		for(int i = 0; i <3; i++)
			Console.Write("{0} ", list[i]);
	}

 }

     
     
 
