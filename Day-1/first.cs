using System;
					
class Program
{
    static void Main(string[] args)
    {
		String InvoiceNo = "[ABC00001]";
		String Name="[EC Group]";
		String Date="[9/9/2020 9:03:32 PM]";
        Double computer=60000.00;
        Double SCharges=38000.50;
        Double OCharges=1999.50;
        Double STotal=100000.00;
        Double tax=5000.00;
        Double discount=3150.00;
        Double total=101850.00;
		Console.WriteLine("===========Invoice header============");
		Console.WriteLine("Invoice No:" + InvoiceNo);
		Console.WriteLine("Customer Name:" + Name);
		Console.WriteLine("Invoice Date:" + Date);
		Console.WriteLine("===========Invoice Details===========");
        Console.WriteLine("Computers: " + computer); 
        Console.WriteLine("Service charges: " + SCharges);
        Console.WriteLine("Other charges: " + OCharges);
        Console.WriteLine("Sub Total: " + STotal);
        Console.WriteLine("Tax[5%] : " + tax);
        Console.WriteLine("Discount[3%] : " + discount);
        Console.WriteLine("Total : " + total);
		
    }
}